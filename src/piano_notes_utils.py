MIDI_12_NOTE_CODE_TO_UNIQUE_GUID={
60 :"b5b88bff-40b1-4c44-b3ca-3e4cbf02dba2",
61 :"ce690d4a-9e67-4ceb-a1a5-ddbe17292c56",
62 :"0b272e3d-f911-4d75-9986-9bb2dfd365dd",
63 :"854c09ac-8a5c-4081-a0a7-a413d8554c2b",
64 :"2216e47b-c9fa-4a73-a691-c9d08dc9fab2",
65 :"45748312-d9f7-43f0-a417-889b04991c9b",
66 :"44ead7ba-440b-402f-94ed-85b415200f51",
67 :"2a42413e-3439-4b2e-a4c1-1417e2bff8cb",
68 :"c94d9ec3-e7b8-4183-ab8d-bad9c6200c6b",
69 :"367440e7-2db7-4424-9eee-b2e86905fb8c",
70 :"ff29369b-c611-45ad-909a-21752f3c2cc6",
71 :"75cc350c-e4fa-4bbe-96c7-1b935e001489",
}

MIDI_88_NOTE_CODE_TO_UNIQUE_GUID={
21 :"3c9da2cc-8e8d-4b87-8d79-5e45138d392b",
22 :"244935b3-209f-4306-ba55-b2d273e33004",
23 :"5f58b883-e626-4c89-ac88-372bb6ff36bc",
24 :"d2bc4634-a17b-46a3-93ce-04096ffada62",
25 :"c60b42ac-fc65-499c-b47e-e0172d98364f",
26 :"0baa8276-9e83-49d4-b5a9-accce7fb4cdb",
27 :"23e9a32a-378e-44ba-ab28-ec3896880cf7",
28 :"bfdf2fd9-4779-40e4-a00f-81b927d2e9ea",
29 :"eec5f1b2-d8e9-4575-af3c-026342f4e648",
30 :"37ba1bcb-e1ad-47df-9fe5-e0156694e371",
31 :"7b1180be-1924-4f8a-b925-69c719416270",
32 :"16c9bb0e-7f7c-4405-857f-26da6d575958",
33 :"bf9d8dee-6616-413a-806b-ef78c3a89441",
34 :"faf329e3-c1e4-4c74-b54d-ffae6b3fe77a",
35 :"5f21bbe2-38ef-46a0-b98e-b7bf90d572bb",
36 :"da9a2fbb-4e0b-4ae1-a135-dc2a5cc9bce0",
37 :"a9ee0d91-843a-4660-a61f-01a1ae4e000b",
38 :"9340709c-570a-42e0-b02c-7423195cb28b",
39 :"ab6103e1-1691-44c3-a0c6-58df2878bd47",
40 :"adc4c9df-a070-48ee-8b58-c3895b79f055",
41 :"0403bd45-3c53-45a6-868c-92873275b6d9",
42 :"262cdc31-cfef-4651-a072-186b02188f3f",
43 :"a6fc881e-6692-447e-879a-a2f87d38e5bf",
44 :"d220838b-1242-4f35-983b-15c6935d51ca",
45 :"26884f94-53fb-4003-80a0-aaba907aa06c",
46 :"e5397136-f0aa-4f59-b5e3-dc8114238923",
47 :"903023a8-f4e2-406d-853b-11a8a2bf9c95",
48 :"0ca56a48-86c3-43a6-b67e-48bc743635ff",
49 :"16df2cff-44ee-4ce7-b514-bdaefa355cf0",
50 :"de478604-16ca-4beb-96bc-8a3aafcfbf5f",
51 :"a1f6643f-411e-497f-8b20-b54e70796955",
52 :"362096c4-db4b-4e03-85f8-c0de95144fd4",
53 :"80f4a2a3-e8ec-4803-878c-23b2dc439296",
54 :"7200ebdd-2759-403f-8096-6852ad88afaa",
55 :"a239f12b-774a-479c-8556-d01739f1ee99",
56 :"754fa224-e8cc-4cd3-9c73-9dab1e79deaf",
57 :"c9c9ac2b-9a8a-413d-bed2-393b7c0a4e17",
58 :"a7889dcf-d7f8-48a7-b6a9-b898fc4413dd",
59 :"e6408d15-ae4b-4082-9662-1a57415fcc2d",
60 :"4e26deb8-f8b9-4e84-9376-8c34ddbf2c44",
61 :"8ae540eb-57fe-4b10-b7e5-af4d02376252",
62 :"58cef27a-e4d0-4221-b503-5aef0bd58736",
63 :"f152c6f8-7762-4752-b08b-f4a8bf33ccf4",
64 :"8680739f-d7b4-4092-8113-d3d2d1f9aaea",
65 :"5f925c50-304c-48e8-95cd-2a927905c044",
66 :"36e5821d-8a96-47b9-b383-66fabcd46bb1",
67 :"1365cf57-f2f3-46e6-b8e7-bd19a66a283a",
68 :"d5b6dd8a-cba9-4464-affb-fa0446c48123",
69 :"66a6a2cb-c4db-4725-9515-9ae18eb476e6",
70 :"d9dec0f5-0709-44b8-90bf-404b1d30e5e0",
71 :"419db928-5dd1-4937-ba7f-551f7da3aeea",
72 :"661f41af-97c5-4786-b428-6c835af959ac",
73 :"2b0c3c94-b286-4eed-b93c-7ab1649a5065",
74 :"0de822cf-df74-41c7-a3e6-cdd93bed8d09",
75 :"dff4a135-3a07-42d8-ace3-aec1a136b527",
76 :"8b5bb008-bc92-43fd-b8a2-07cac1120e56",
77 :"436b8ce9-c3ec-445e-81cc-b220ffe8a271",
78 :"0848fc42-a6d3-40da-93d0-eddd7d06361e",
79 :"ca4406d4-0211-4714-818b-82dc4a41b0a1",
80 :"0daff05f-e738-4a86-a218-5303fcc87e20",
81 :"bc1ef30f-f2f9-418d-b9db-73d620c82078",
82 :"f58631b1-1e5b-42ee-bd65-d0a1ed16988a",
83 :"8f656b44-29b3-4889-ae9c-78a070bdaaec",
84 :"cb7a7c9f-8657-45ba-b08d-e150801fd0a4",
85 :"5848071d-af63-4f5f-8f5f-5dbd958b7d50",
86 :"e3a92262-35a0-49b5-8240-c93f284bb9dc",
87 :"ca6654a9-2a37-44e1-bab1-ba75a60e8ac7",
88 :"92c542ab-9d07-46d1-b906-5cf18d00f470",
89 :"937bb254-0c47-4cb5-92db-19710562390d",
90 :"db3406d6-59ed-44c4-9c67-5f604546c0a8",
91 :"233d1e6d-a3e4-4f02-bcaa-2e3283fe6da5",
92 :"57707982-1edd-4ca2-9a6d-c36a3eeab711",
93 :"e71c6d10-f4b1-4c5b-a39e-564192bd0404",
94 :"adba30e2-788c-4771-8cf1-11747e6979e0",
95 :"b09c5525-3081-454b-a20a-a107b3c6e1ba",
96 :"1da64229-ad40-47c7-8759-eb5ccd75f127",
97 :"3b793133-d726-4dd6-b1c5-7a6317da20c8",
98 :"3f5cbb33-7d17-4381-9eaf-f6797b94668c",
99 :"8855e2b6-cdec-4021-836f-006ddffb0d77",
100:"340177cf-2ad1-42c7-b490-95bfb0fc7458",
101:"05faba57-c1ba-4d3d-9026-0ead0dbc43b9",
102:"0a2c5717-7047-46b6-a587-44d5f4e9707f",
103:"62922b5d-b16b-4c3f-be6e-155157822e8c",
104:"004df53e-755e-4831-b43a-624ffdb09bd4",
105:"703ab7da-5fab-4ac4-9e8c-e695c4b7685d",
106:"ae540473-ca44-4214-a68a-9394d04c1e0a",
107:"874aef0d-e110-4f0f-a3ef-e89514ecb439",
108:"346f1aff-2607-4eac-a8a0-a1d6d68e38a7",
}

MIDI_108_NOTE_CODE_TO_UNIQUE_GUID={
12 :"608567b9-c5f2-49d7-b307-f2c2c909f7cf",
13 :"0669abcd-58c0-44f2-bde6-2b68698e56e0",
14 :"6c741d21-1aab-4d48-aeb2-b2299ca6fad3",
15 :"f7f74e12-1a93-474e-9999-c0bd5960eba6",
16 :"45434945-32a4-48a2-a786-df05d20ccd59",
17 :"1bf880a3-fd14-453e-a9ee-72716d02759f",
18 :"a2a25b20-8805-4d88-97e5-8ba86aa66602",
19 :"6bc3a56b-fc76-4e1a-8c46-5a0ff75daedd",
20 :"279027c6-3267-4a07-b895-ec69699793a9",
21 :"6e7781a8-972b-4149-9c6c-a7adbde4af5f",
22 :"f5a68991-1b0d-4d8a-b2d0-85e8da33cec8",
23 :"a1a33d0d-47f5-4b8e-b36c-66531f4f6f81",
24 :"85031208-a395-437d-a646-c55b6d8ce995",
25 :"b29096b8-13c3-43d7-92d4-caf5cadaf775",
26 :"618d05b3-e595-4c80-8d7e-1d5515b1ff67",
27 :"8646c661-d1b2-4f2b-8702-d938d0aba3b5",
28 :"4b3352b0-5daf-44cd-bd52-ad9978acfba0",
29 :"9c55afc2-fe0c-49a5-8757-a1418f8b7349",
30 :"b8183512-daf2-4ee6-862e-8fb8ea68da52",
31 :"87a7b9d4-0229-492d-9032-d6e6de4f0c89",
32 :"a3ad4a17-ea7c-4fa1-bd04-cf63579c0b0e",
33 :"63169514-02ff-481f-97d4-d8bec0e607b5",
34 :"cfb68803-dada-47f7-ada6-bf7f033111b1",
35 :"6daf1dab-e9e4-4271-be6d-30ca5c2c3998",
36 :"49719b1b-b715-4cec-916c-4ca20316d15d",
37 :"28135aca-ee10-420a-a386-a1f9182d59bc",
38 :"1932b0ff-49af-4efe-ab22-33f5cc09a4e9",
39 :"8f8c983d-56d1-478c-af62-11b9be4ddfc0",
40 :"321bc3ff-ea85-4568-a2d0-b89264d27c4a",
41 :"6748f37d-e1fe-4233-acda-75281f13a37c",
42 :"e7b2eb00-d0a1-4905-a6f0-95b8ddb00d92",
43 :"654e17e9-8a33-4c80-b93e-4b3f20a08df5",
44 :"cbfb7508-d9da-4de4-847a-ce0965595b5a",
45 :"5dbd97ef-fe70-4ed0-8654-a8f00bc96dfe",
46 :"ed0cbe7e-72a6-4be3-9fa0-7b9b33a3996d",
47 :"8145a07f-d6bf-47a1-ac59-b930662c173f",
48 :"1c5bc2bd-2611-4851-b90e-ae7b542fc156",
49 :"0cfec782-e328-4325-bbef-84a5365b6574",
50 :"a7c04344-cc63-4ba4-a79b-7925728685ba",
51 :"323cd1cd-cb12-4995-8fab-c97950556b4f",
52 :"563e8c4f-17f5-41ad-88fa-3cb38a5800b6",
53 :"29771189-e145-4f5d-a6df-37939513cde9",
54 :"395f351e-7b34-40e3-b417-39da0a20fded",
55 :"b39b28ec-8336-454c-a35e-1e6f7e2514aa",
56 :"a7e390af-c923-4836-ba2f-e633e2072ec1",
57 :"eb725aa4-8e85-473d-a775-133c4d15ef06",
58 :"fc14373b-6e94-4e42-971d-91774527d290",
59 :"e8394d5b-2331-4965-b8e4-2f108e810df3",
60 :"ff19b289-6e2e-4198-b85c-e8979bc2ba65",
61 :"26e05f6f-d833-4588-b237-c45a11969f07",
62 :"f8f4a06a-85ff-4430-8f4d-2bed39a6efed",
63 :"99acdda3-2f49-416f-9cdd-4f0a5cfc8c95",
64 :"6516f3e7-0636-41fa-a90f-93adf7c1d871",
65 :"2dfaf116-b168-49bd-9b5a-7227129da0f7",
66 :"bb5c7bee-692e-44dc-a207-54816057de8b",
67 :"bedea6b4-0cd4-4ff9-b985-bf7fa2e59b4f",
68 :"3bc1b265-1644-4c84-9b41-e1250aa3aa53",
69 :"03a134cf-f548-4140-b17b-4b1a117bdb01",
70 :"109bc190-c75c-4eda-9a5c-e334e9d43625",
71 :"b3b05429-3770-4264-8001-594f7e0919f7",
72 :"d8a62bc0-b9bd-4ba8-8617-70d716d5c902",
73 :"bc6d362b-9e70-49ad-8ff0-188beca89b2c",
74 :"acd142e8-b114-46be-a029-ce837b984ec5",
75 :"74cdecdd-d18c-43c5-9592-701640737673",
76 :"9163fea5-a611-410f-b1be-0a4b32483ff6",
77 :"54529b92-5dc0-44ab-b336-8e950cd3e6a2",
78 :"b8639803-6c53-4284-870d-49c9849ff9e8",
79 :"06a3eb7f-b0e4-4a17-b896-fb70dd592bfe",
80 :"ebf08832-c28d-4366-b893-5cc5ee465572",
81 :"d1c4746c-4bdc-453f-9e04-bbf94cfe5fd5",
82 :"8b51c488-2295-43da-a04e-fb66122e0a5a",
83 :"b76185d3-91d9-487c-8a62-a1371f188a71",
84 :"4f48980c-9640-4c0a-9c97-608cecf3383e",
85 :"93014bc3-632b-4612-990f-3566723811f4",
86 :"6e84dcd4-1023-4ee9-b7ba-80903c476fe3",
87 :"f866d45a-4a57-4270-adf9-874d193a36ad",
88 :"5210987a-2f3a-43b7-a877-a1f98b12c30a",
89 :"b946960f-9d45-4f5b-9b87-0784cea7757c",
90 :"bf19493d-fd6a-47bc-9299-96aba84373d7",
91 :"197f86b0-a83b-4d21-ba6c-7cb23d15bc13",
92 :"a77159be-f258-49ae-bd57-c84dc1ea74fc",
93 :"ab087d8e-b5f9-4bfd-a5cc-7fe752e805c7",
94 :"90f7064d-7a70-44f9-9149-1c21f0ac4ecb",
95 :"8f325a11-9b26-4158-a349-6c2b09f0c484",
96 :"6007ebdd-a75e-4e37-b3a4-a80636ffbbd4",
97 :"b95e28b0-5bdc-4989-bbe7-de225b099e3f",
98 :"4a7013bf-5f29-4f97-9187-37a069c5785e",
99 :"426e0b27-8efb-4570-91c9-a52be6eb272c",
100:"7f1f05e2-51c0-4cc8-8b6b-b36dd71f6562",
101:"50c89cf7-cbe9-4e06-afc2-caa3958d6289",
102:"6f577f09-15c8-4a50-bd6b-5bd875043160",
103:"8352bbd6-e559-4723-9046-b20d470f466a",
104:"6718c0ae-0a5f-43da-a090-924cd0dd9eed",
105:"84e35ffd-45e1-49ec-999e-ac880e083d52",
106:"80aa3456-2ad5-4b43-9d08-0575c512bc1b",
107:"4e6cf64f-40f8-4b19-b31b-0bbf96cf5f1d",
108:"489fcf40-d870-4604-8432-3f7788bffe57",
109:"b1cfa702-d974-4303-b964-e706ea70d215",
110:"8b5ad5e3-0782-42be-bf79-e1fbbfde46bd",
111:"e9a0ecc1-f577-4bf1-a189-6117ead5a32e",
112:"b2fbc92c-05dc-4805-a557-62cc62a72297",
113:"ee860fdf-c6a8-4208-be10-82e4b655c9bd",
114:"bd18f103-212b-4260-af10-551cfd62184a",
115:"c253cf7b-b1ca-47e7-bb77-71b03d2fa2be",
116:"b5924fce-d1ca-4083-99e0-8bcaec2ac002",
117:"b887510d-9d84-4489-8b1a-4e4360435938",
118:"9efea10e-2b01-4f29-bf8a-3499bc7af4e8",
119:"cff4f664-1520-4eae-9cf3-0a039610f0c1",
}

MIDI_NOTE_CODE_TO_FREQUENCY={
12 :16.35160,
13 :17.32391,
14 :18.35405,
15 :19.44544,
16 :20.60172,
17 :21.82676,
18 :23.12465,
19 :24.49971,
20 :25.95654,
21 :27.5,
22 :29.13524,
23 :30.86771,
24 :32.70320,
25 :34.64783,
26 :36.70810,
27 :38.89087,
28 :41.20344,
29 :43.65353,
30 :46.24930,
31 :48.99943,
32 :51.91309,
33 :55,
34 :58.27047,
35 :61.73541,
36 :65.40639,
37 :69.29566,
38 :73.41619,
39 :77.78175,
40 :82.40689,
41 :87.30706,
42 :92.49861,
43 :97.99886,
44 :103.8262,
45 :110,
46 :116.5409,
47 :123.4708,
48 :130.8128,
49 :138.5913,
50 :146.8324,
51 :155.5635,
52 :164.8138,
53 :174.6141,
54 :184.9972,
55 :195.9977,
56 :207.6523,
57 :220,
58 :233.0819,
59 :246.9417,
60 :261.6256,
61 :277.1826,
62 :293.6648,
63 :311.1270,
64 :329.6276,
65 :349.2282,
66 :369.9944,
67 :391.9954,
68 :415.3047,
69 :440,
70 :466.1638,
71 :493.8833,
72 :523.2511,
73 :554.3653,
74 :587.3295,
75 :622.2540,
76 :659.2551,
77 :698.4565,
78 :739.9888,
79 :783.9909,
80 :830.6094,
81 :880,
82 :932.3275,
83 :987.7666,
84 :1046.502,
85 :1108.731,
86 :1174.659,
87 :1244.508,
88 :1318.510,
89 :1396.913,
90 :1479.978,
91 :1567.982,
92 :1661.219,
93 :1760,
94 :1864.655,
95 :1975.533,
96 :2093.005,
97 :2217.461,
98 :2349.318,
99 :2489.016,
100:2637.020,
101:2793.826,
102:2959.955,
103:3135.963,
104:3322.438,
105:3520,
106:3729.310,
107:3951.066,
108:4186.009,
109:4434.922,
110:4698.636,
111:4978.032,
112:5274.041,
113:5587.652,
114:5919.911,
115:6271.927,
116:6644.875,
117:7040,
118:7458.620,
119:7902.133,
}

MIDI_NOTE_CODE_TO_SCIENTIFIC_NOTE_NAME={
12 :"C0 / B♯-1",
13 :"C♯0 / D♭0",
14 :"D0",
15 :"D♯0 / E♭0",
16 :"E0 / F♭0",
17 :"F0 / E♯0",
18 :"F♯0 / G♭0",
19 :"G0",
20 :"G♯0 / A♭0",
21 :"A0",
22 :"A♯0 / B♭0",
23 :"B0 / C♭1",
24 :"C1 / B♯0",
25 :"C♯1 / D♭1",
26 :"D1",
27 :"D♯1 / E♭1",
28 :"E1 / F♭1",
29 :"F1 / E♯1",
30 :"F♯1 / G♭1",
31 :"G1",
32 :"G♯1 / A♭1",
33 :"A1",
34 :"A♯1 / B♭1",
35 :"B1 / C♭2",
36 :"C2 / B♯1",
37 :"C♯2 / D♭2",
38 :"D2",
39 :"D♯2 / E♭2",
40 :"E2 / F♭2",
41 :"F2 / E♯2",
42 :"F♯2 / G♭2",
43 :"G2",
44 :"G♯2 / A♭2",
45 :"A2",
46 :"A♯2 / B♭2",
47 :"B2 / C♭3",
48 :"C3 / B♯2",
49 :"C♯3 / D♭3",
50 :"D3",
51 :"D♯3 / E♭3",
52 :"E3 / F♭3",
53 :"F3 / E♯3",
54 :"F♯3 / G♭3",
55 :"G3",
56 :"G♯3 / A♭3",
57 :"A3",
58 :"A♯3 / B♭3",
59 :"B3 / C♭4",
60 :"C4 / B♯3",
61 :"C♯4 / D♭4",
62 :"D4",
63 :"D♯4 / E♭4",
64 :"E4 / F♭4",
65 :"F4 / E♯4",
66 :"F♯4 / G♭4",
67 :"G4",
68 :"G♯4 / A♭4",
69 :"A4",
70 :"A♯4 / B♭4",
71 :"B4 / C♭5",
72 :"C5 / B♯4",
73 :"C♯5 / D♭5",
74 :"D5",
75 :"D♯5 / E♭5",
76 :"E5 / F♭5",
77 :"F5 / E♯5",
78 :"F♯5 / G♭5",
79 :"G5",
80 :"G♯5 / A♭5",
81 :"A5",
82 :"A♯5 / B♭5",
83 :"B5 / C♭6",
84 :"C6 / B♯5",
85 :"C♯6 / D♭6",
86 :"D6",
87 :"D♯6 / E♭6",
88 :"E6 / F♭6",
89 :"F6 / E♯6",
90 :"F♯6 / G♭6",
91 :"G6",
92 :"G♯6 / A♭6",
93 :"A6",
94 :"A♯6 / B♭6",
95 :"B6 / C♭7",
96 :"C7 / B♯6",
97 :"C♯7 / D♭7",
98 :"D7",
99 :"D♯7 / E♭7",
100:"E7 / F♭7",
101:"F7 / E♯7",
102:"F♯7 / G♭7",
103:"G7",
104:"G♯7 / A♭7",
105:"A7",
106:"A♯7 / B♭7",
107:"B7 / C♭8",
108:"C8 / B♯7",
109:"C♯8 / D♭8",
110:"D8",
111:"D♯8 / E♭8",
112:"E8 / F♭8",
113:"F8 / E♯8",
114:"F♯8 / G♭8",
115:"G8",
116:"G♯8 / A♭8",
117:"A8",
118:"A♯8 / B♭8",
119:"B8 / C♭9",
}

MIDI_NOTE_CODE_TO_HELMHOLTZ_NOTE_NAME={
12 :"͵͵C / ͵͵͵H♯",
13 :"͵͵C♯ / ͵͵D♭",
14 :"͵͵D0",
15 :"͵͵D♯ / ͵͵E♭",
16 :"͵͵E / ͵͵F♭",
17 :"͵͵F / ͵͵E♯",
18 :"͵͵F♯ / ͵͵G♭",
19 :"͵͵G",
20 :"͵͵G♯ / ͵͵A♭",
21 :"͵͵A",
22 :"͵͵A♯ / ͵͵H♭",
23 :"͵͵H / ͵C♭1",
24 :"͵C / ͵͵H♯",
25 :"͵C♯ / ͵D♭",
26 :"͵D",
27 :"͵D♯ / ͵E♭",
28 :"͵E / ͵F♭",
29 :"͵F / ͵E♯",
30 :"͵F♯ / ͵G♭",
31 :"͵G",
32 :"͵G♯ / ͵A♭",
33 :"͵A",
34 :"͵A♯ / ͵H♭",
35 :"͵H / C♭",
36 :"C / ͵H♯",
37 :"C♯ / D♭",
38 :"D",
39 :"D♯ / E♭",
40 :"E / F♭",
41 :"F / E♯",
42 :"F♯ / G♭",
43 :"G",
44 :"G♯ / A♭",
45 :"A",
46 :"A♯ / H♭",
47 :"H / c♭",
48 :"c / H♯",
49 :"c♯ / b♭",
50 :"d",
51 :"d♯ / e♭",
52 :"e / f♭",
53 :"f / e♯",
54 :"f♯ / g♭",
55 :"g",
56 :"g♯ / a♭",
57 :"a",
58 :"a♯ / h♭",
59 :"h / c♭′",
60 :"c′ / h♯",
61 :"c♯′ / d♭′",
62 :"d′",
63 :"d♯′ / e♭′",
64 :"e′ / f♭′",
65 :"f′ / e♯′",
66 :"f♯′ / g♭′",
67 :"g′",
68 :"g♯′ / a♭′",
69 :"a′",
70 :"a♯′ / g♭′",
71 :"h′ / c♭′′",
72 :"c′′ / h♯′",
73 :"c♯′′ / d♭′′",
74 :"d′′",
75 :"d♯′′ / e♭′′",
76 :"e′′ / f♭′′",
77 :"f′′ / e♯′′",
78 :"f♯′′ / g♭′′",
79 :"g′′",
80 :"g♯′′ / a♭′′",
81 :"a′′",
82 :"a♯′′ / h♭′′",
83 :"h′′ / c♭′′′",
84 :"c′′′ / h♯′′",
85 :"c♯′′′ / d♭′′′",
86 :"d′′′",
87 :"d♯′′′ / e♭′′′",
88 :"e′′′ / f♭′′′",
89 :"f′′′ / e♯′′′",
90 :"f♯′′′ / g♭′′′",
91 :"g′′′",
92 :"g♯′′′ / a♭′′′",
93 :"a′′′",
94 :"a♯′′′ / h♭′′′",
95 :"h′′′ / c♭′′′′",
96 :"c′′′′ / h♯′′′",
97 :"c♯′′′′ / d♭′′′′",
98 :"d′′′′",
99 :"d♯′′′′ / e♭′′′′",
100:"e′′′′ / f♭′′′′",
101:"f′′′′ / e♯′′′′",
102:"f♯′′′′ / g♭′′′′",
103:"g′′′′",
104:"g♯′′′′ / a♭′′′′",
105:"a′′′′",
106:"a♯′′′′ / h♭′′′′",
107:"h′′′′ / c♭′′′′′",
108:"c′′′′′ / h♯′′′′",
109:"c♯′′′′′ / d♭′′′′′",
110:"d′′′′′",
111:"d♯′′′′′ / e♭′′′′′",
112:"e′′′′′ / f♭′′′′′",
113:"f′′′′′ / e♯′′′′′",
114:"f♯′′′′′ / g♭′′′′′",
115:"g′′′′′",
116:"g♯′′′′′ / a♭′′′′′",
117:"a′′′′′",
118:"a♯′′′′′ / h♭′′′′′",
119:"h′′′′′ / c♭′′′′′′",
}

MIDI_NOTE_CODE_TO_LATIN_NOTE_NAME={
12 :"Do-2 / Si♯-3",
13 :"Do♯-2 / Ré♭-2",
14 :"Ré-2",
15 :"Ré♯-2 / Mi♭-2",
16 :"Mi-2 / Fa♭-2",
17 :"Fa-2 / Mi♯-2",
18 :"Fa♯-2 / Sol♭-2",
19 :"Sol-2",
20 :"Sol♯-2 / La♭-2",
21 :"La-2",
22 :"La♯-2 / Si♭-2",
23 :"Si-2 / Do♭-1",
24 :"Do-1 / Si♯-2",
25 :"Do♯-1 / Ré♭-1",
26 :"Ré-1",
27 :"Ré♯-1 / Mi♭-1",
28 :"Mi-1 / Fa♭-1",
29 :"Fa-1 / Mi♯-1",
30 :"Fa♯-1 / Sol♭-1",
31 :"Sol-1",
32 :"Sol♯-1 / La♭-1",
33 :"La-1",
34 :"La♯-1 / Si♭-1",
35 :"Si-1 / Do♭1",
36 :"Do1 / Si♯-1",
37 :"Do♯1 / Ré♭1",
38 :"Ré1",
39 :"Ré♯1 / Mi♭1",
40 :"Mi1 / Fa♭1",
41 :"Fa1 / Mi♯1",
42 :"Fa♯1 / Sol♭1",
43 :"Sol1",
44 :"Sol♯1 / La♭1",
45 :"La1",
46 :"La♯1 / Si♭1",
47 :"Si1 / Do♭2",
48 :"Do2 / Si♯1",
49 :"Do♯2 / Ré♭2",
50 :"Ré2",
51 :"Ré♯2 / Mi♭2",
52 :"Mi2 / Fa♭2",
53 :"Fa2 / Mi♯2",
54 :"Fa♯2 / Sol♭2",
55 :"Sol2",
56 :"Sol♯2 / La♭2",
57 :"La2",
58 :"La♯2 / Si♭2",
59 :"Si2 / Do♭3",
60 :"Do3 / Si♯2",
61 :"Do♯3 / Ré♭3",
62 :"Ré3",
63 :"Ré♯3 / Mi♭3",
64 :"Mi3 / Fa♭3",
65 :"Fa3 / Mi♯3",
66 :"Fa♯3 / Sol♭3",
67 :"Sol3",
68 :"Sol♯3 / La♭3",
69 :"La3",
70 :"La♯3 / Si♭3",
71 :"Si3 / Do♭4",
72 :"Do4 / Si♯3",
73 :"Do♯4 / Ré♭4",
74 :"Ré4",
75 :"Ré♯4 / Mi♭4",
76 :"Mi4 / Fa♭4",
77 :"Fa4 / Mi♯4",
78 :"Fa♯4 / Sol♭4",
79 :"Sol4",
80 :"Sol♯4 / La♭4",
81 :"La4",
82 :"La♯4 / Si♭4",
83 :"Si4 / Do♭5",
84 :"Do5 / Si♯4",
85 :"Do♯5 / Ré♭5",
86 :"Ré5",
87 :"Ré♯5 / Mi♭5",
88 :"Mi5 / Fa♭5",
89 :"Fa5 / Mi♯5",
90 :"Fa♯5 / Sol♭5",
91 :"Sol5",
92 :"Sol♯5 / La♭5",
93 :"La5",
94 :"La♯5 / Si♭5",
95 :"Si5 / Do♭6",
96 :"Do6 / Si♯5",
97 :"Do♯6 / Ré♭6",
98 :"Ré6",
99 :"Ré♯6 / Mi♭6",
100:"Mi6 / Fa♭6",
101:"Fa6 / Mi♯6",
102:"Fa♯6 / Sol♭6",
103:"Sol6",
104:"Sol♯6 / La♭6",
105:"La6",
106:"La♯6 / Si♭6",
107:"Si6 / Do♭7",
108:"Do7 / Si♯6",
109:"Do♯7 / Ré♭7",
110:"Ré7",
111:"Ré♯7 / Mi♭7",
112:"Mi7 / Fa♭7",
113:"Fa7 / Mi♯7",
114:"Fa♯7 / Sol♭7",
115:"Sol7",
116:"Sol♯7 / La♭7",
117:"La7",
118:"La♯7 / Si♭7",
119:"Si7 / Do♭8",
}

