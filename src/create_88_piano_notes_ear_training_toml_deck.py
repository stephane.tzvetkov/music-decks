import pathlib
import shutil
from midiutil import MIDIFile
from midi2audio import FluidSynth
#from time import sleep

import os
import piano_notes_utils as PNU

DECK_NAME="88 piano notes ear training"
DIR_LOCATION_OF_THIS_SCRIPT=os.path.dirname(os.path.realpath(__file__))

EXPORT_LOCATION=f"{DIR_LOCATION_OF_THIS_SCRIPT}/../decks/{DECK_NAME}"
shutil.rmtree(path = f"{EXPORT_LOCATION}", ignore_errors = True)
pathlib.Path(f"{EXPORT_LOCATION}/_media").mkdir(parents=True, exist_ok=True)

with open(f"{EXPORT_LOCATION}/{DECK_NAME}.toml", "a") as file:
    file.write(f"""deck="{DECK_NAME}"\n""")

for midi_note_code in range(21, 109):
    # Create MIDIFile object
    MyMIDI = MIDIFile(1)

    # Add track name and tempo
    track = 0
    time = 0
    tempo = 120
    MyMIDI.addTrackName(track, time, f"Piano note {midi_note_code}")
    MyMIDI.addTempo(track, time, tempo)

    # Add note with channel, duration, and volume
    channel = 0
    duration = 16
    volume = 100
    MyMIDI.addNote(track, channel, midi_note_code, time, duration, volume)

    # Write track/note to MIDI file
    with open(f"{EXPORT_LOCATION}/_media/{midi_note_code}.mid", 'wb') as binfile:
        MyMIDI.writeFile(binfile)

    #sleep(0.1)

    # Convert MIDI to MP3 (using the default sound font in 44100 Hz sample rate)
    fs = FluidSynth('/usr/share/soundfonts/default.sf2')
    fs.midi_to_audio(f"{EXPORT_LOCATION}/_media/{midi_note_code}.mid", f"{EXPORT_LOCATION}/_media/{midi_note_code}.mp3")

    # Convert MIDI to WAV (using the default sound font in 44100 Hz sample rate)
    #fs.midi_to_audio(f"{EXPORT_LOCATION}/_media/{midi_note_code}.mid", f"{EXPORT_LOCATION}/_media/{midi_note_code}.wav")

    # Convert MIDI to FLAC (lossless codec)
    #fs.midi_to_audio(f"{EXPORT_LOCATION}/_media/{midi_note_code}.mid", f"{EXPORT_LOCATION}/_media/{midi_note_code}.flac")

    TOML_NOTE=f"""
[note_{PNU.MIDI_88_NOTE_CODE_TO_UNIQUE_GUID[midi_note_code]}]
guid="{PNU.MIDI_88_NOTE_CODE_TO_UNIQUE_GUID[midi_note_code]}"
tags=""
front='''
What is this note?
[sound:{midi_note_code}.mp3]
'''
back='''
{PNU.MIDI_NOTE_CODE_TO_SCIENTIFIC_NOTE_NAME[midi_note_code]} (scientific/international notation)
{PNU.MIDI_NOTE_CODE_TO_HELMHOLTZ_NOTE_NAME[midi_note_code]} (helmholtz notation)
{PNU.MIDI_NOTE_CODE_TO_LATIN_NOTE_NAME[midi_note_code]} (latin notation)
{PNU.MIDI_NOTE_CODE_TO_FREQUENCY[midi_note_code]} Hz (equal temperament frenquency)
{midi_note_code} (MIDI note code)
'''
"""

    with open(f"{EXPORT_LOCATION}/{DECK_NAME}.toml", "a") as file:
        file.write(TOML_NOTE)
