import os
from midiutil import MIDIFile
from midi2audio import FluidSynth

# See https://onlinesequencer.net/2433586#

# One track, defaults to format 1 (tempo track is created automatically)
MyMIDI = MIDIFile(1)

# track TODO
# time TODO
# tempo in beats per minute
#MyMIDI.addTempo(track=0, time=0, tempo=125)
MyMIDI.addTempo(0, 0, 125)

# track TODO
# channel TODO
# pitch is in midi code
# time is in beat(s)
# duration is in beat(s)
# volume from 0 to 127
#
#MyMIDI.addNote(track, channel, pitch, time, duration, volume)

# E7 - e′′′′ - Mi6     : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=100, time=0, duration=0.25, volume=127)
# D7 - d′′′′ - Ré6     : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=98, time=0.25, duration=0.25, volume=127)

# F#6 - f#′′′ - Fa#5   : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=90, time=0.5, duration=0.5, volume=127)
# G#6 - g#′′′ - Sol#5  : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=92, time=1, duration=0.5, volume=127)

# C#7 - c#′′′′ - Do#6  : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=97, time=1.5, duration=0.25, volume=127)
# B6 - h′′′ - Si5      : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=95, time=1.75, duration=0.25, volume=127)

# D6 - d′′′ - Ré5      : for 2 beats 
MyMIDI.addNote(track=0, channel=0, pitch=86, time=2, duration=0.5, volume=127)
# E6 - e′′′ - Mi5      : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=88, time=2.5, duration=0.5, volume=127)

# B6 - h′′′ - Si5      : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=95, time=3, duration=0.25, volume=127)
# A6 - a′′′ - La5      : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=93, time=3.25, duration=0.25, volume=127)

# C#6 - c#′′′ - Do#5   : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=85, time=3.5, duration=0.5, volume=127)
# E6 - e′′′ - Mi5      : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=88, time=4, duration=0.5, volume=127)

# A6 - a′′′ - La5      : for 6 beats
MyMIDI.addNote(track=0, channel=0, pitch=93, time=4.5, duration=1.5, volume=127)

DIR_LOCATION_OF_THIS_SCRIPT=os.path.dirname(os.path.realpath(__file__))
EXPORT_LOCATION=f"{DIR_LOCATION_OF_THIS_SCRIPT}/../decks/Famous ringtones"

with open(f"{EXPORT_LOCATION}/_media/nokia_ringtone.mid", 'wb') as binfile:
    MyMIDI.writeFile(binfile)

# Convert MIDI to MP3 (using the default sound font in 44100 Hz sample rate)
fs = FluidSynth('/usr/share/soundfonts/default.sf2')
fs.midi_to_audio(
                f"{EXPORT_LOCATION}/_media/nokia_ringtone.mid", 
                f"{EXPORT_LOCATION}/_media/nokia_ringtone.mp3"
                )
