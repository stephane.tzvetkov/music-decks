# See https://onlinesequencer.net/3573851

import os
from midiutil import MIDIFile
from midi2audio import FluidSynth

# See https://onlinesequencer.net/2433586#

# One track, defaults to format 1 (tempo track is created automatically)
MyMIDI = MIDIFile(1)

# track TODO
# time TODO
# tempo in beats per minute
#MyMIDI.addTempo(track=0, time=0, tempo=125)
MyMIDI.addTempo(0, 0, 80)

# track TODO
# channel TODO
# pitch is in midi code
# time is in beat(s)
# duration is in beat(s)
# volume from 0 to 127
#
#MyMIDI.addNote(track, channel, pitch, time, duration, volume)

# F4 - f′ - Fa3   : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=65, time=0, duration=1, volume=127)
# A4 - a′ - La3   : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=69, time=1, duration=1, volume=127)
# G4 - g′ - Sol3  : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=67, time=2, duration=1, volume=127)
# C4 - c′ - Do3   : for 2 beats
MyMIDI.addNote(track=0, channel=0, pitch=60, time=3, duration=2, volume=127)

# F4 - f′ - Fa3   : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=65, time=5.5, duration=1, volume=127)
# G4 - g′ - Sol3  : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=67, time=6.5, duration=1, volume=127)
# A4 - a′ - La3   : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=69, time=7.5, duration=1, volume=127)
# F4 - f′ - Fa3   : for 1 beat
MyMIDI.addNote(track=0, channel=0, pitch=65, time=8.5, duration=2, volume=127)

DIR_LOCATION_OF_THIS_SCRIPT=os.path.dirname(os.path.realpath(__file__))
EXPORT_LOCATION=f"{DIR_LOCATION_OF_THIS_SCRIPT}/../decks/Famous ringtones"

with open(f"{EXPORT_LOCATION}/_media/big_ben_chimes.mid", 'wb') as binfile:
    MyMIDI.writeFile(binfile)

# Convert MIDI to MP3 (using the default sound font in 44100 Hz sample rate)
fs = FluidSynth('/usr/share/soundfonts/default.sf2')
fs.midi_to_audio(
                f"{EXPORT_LOCATION}/_media/big_ben_chimes.mid", 
                f"{EXPORT_LOCATION}/_media/big_ben_chimes.mp3"
                )
