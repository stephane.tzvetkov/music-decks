# TODO

- [v] Decks to train the ear to recognize piano notes
    - [ ] Also add notes with the notation on a partition
    - [ ] Also add notes with the location on a keyboard
- [ ] DAW keyboard shortcuts
- [ ] Deck(s) about music theory
- [ ] Deck(s) about music history
- [ ] Deck(s) to train the ear to recognize piano intervals (see the "All Music Intervals" deck)
- [ ] Deck(s) to train the ear to recognize members of piano chords
- [ ] Deck(s) to train the ear to recognize different tempo/bpm

⭐️ See <https://ankiweb.net/shared/info/1250336138>

See <https://ankiweb.net/shared/decks?search=Music&sort=rating>
