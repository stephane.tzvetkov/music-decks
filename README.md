# Music Anki deck

WIP/TODO

## Contribute

Prerequisite:

- install git
- install tidy
- install poetry
- optional: install direnv

Build:
```console
$ poetry run python3 ./src/create_12_piano_notes_ear_training_toml_deck.py
$ poetry run toml2anki --input-toml-file decks/12\ piano\ notes\ ear\ training/12\ piano\ notes\ ear\ training.toml --input-media-directory decks/12\ piano\ notes\ ear\ training/_media --output-directory ./build

$ poetry run python3 ./src/create_88_piano_notes_ear_training_toml_deck.py
$ poetry run toml2anki --input-toml-file decks/88\ piano\ notes\ ear\ training/88\ piano\ notes\ ear\ training.toml --input-media-directory decks/88\ piano\ notes\ ear\ training/_media --output-directory ./build

$ poetry run python3 ./src/create_108_piano_notes_ear_training_toml_deck.py
$ poetry run toml2anki --input-toml-file decks/108\ piano\ notes\ ear\ training/108\ piano\ notes\ ear\ training.toml --input-media-directory decks/108\ piano\ notes\ ear\ training/_media --output-directory ./build
```

WIP/TODO
